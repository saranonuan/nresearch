from mongoengine import *

from config import MONGO_HOST, MONGO_COLLECTION

connect(MONGO_COLLECTION, host=MONGO_HOST)


class TwitterList(Document):
    user_id = IntField(required=True)
    list_id = IntField(required=True)
    data = DictField()
