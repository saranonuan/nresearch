#!/usr/bin/env python
# -*- coding: utf-8 -*-
import logging
from time import sleep

import requests
import tweepy
import codecs
import sys
from datetime import datetime

from pymongo.errors import ServerSelectionTimeoutError
from tweepy.binder import bind_api
from tweepy.parsers import JSONParser
from config import CONSUMER_KEY, CONSUMER_SECRET
from model import TwitterList

USER_ID = sys.argv[1]
LIMIT = sys.argv[2] if len(sys.argv) > 2 else 100
START_CURSOR = sys.argv[3] if len(sys.argv) > 3 else -1

sys.stdin = codecs.getreader('utf-8')(sys.stdin)
sys.stdout = codecs.getwriter('utf-8')(sys.stdout)

formatter = logging.Formatter('%(asctime)-15s  %(message)s')
stream_log = logging.StreamHandler()
stream_log.setFormatter(formatter)
file_log = logging.FileHandler(filename='logs/%s.log' % datetime.now())
file_log.setFormatter(formatter)

logger = logging.getLogger('NResearch')
logger.setLevel(logging.INFO)
logger.addHandler(stream_log)
logger.addHandler(file_log)
tweepy_log = logging.getLogger('tweepy.binder')
tweepy_log.setLevel(logging.INFO)
tweepy_log.addHandler(stream_log)
tweepy_log.addHandler(file_log)


def main():
    start_time = datetime.now()
    logger.info("START AT %s" % start_time)

    logger.info("Authenticate user")
    logger.info("CK : %s" % CONSUMER_KEY)
    logger.info("CS : %s" % CONSUMER_SECRET)
    auth = tweepy.AppAuthHandler(CONSUMER_KEY, CONSUMER_SECRET)

    api_args = {
        'wait_on_rate_limit': True,
        'wait_on_rate_limit_notify': True,
        'retry_count': 999,
        'retry_delay': 15,
        'parser': JSONParser()
    }

    logger.info("Connect API with arguments : %s" % api_args)
    api = tweepy.API(auth, **api_args)

    target_method = bind_api(
            api=api,
            path='/lists/memberships.json',
            payload_type='list', payload_list=True,
            allowed_param=['screen_name', 'user_id', 'filter_to_owned_lists', 'cursor', 'count'],
            require_auth=True
        )

    finish = False
    next_cursor = START_CURSOR

    while not finish:
        method_args = {'user_id': USER_ID, 'count': LIMIT, 'cursor': next_cursor}
        cursor = tweepy.Cursor(target_method, **method_args)
        iterator = cursor.pages()
        logger.info("Start query at cursor : %d" % next_cursor)

        try:
            for page in iterator:
                logger.info("Query Page : %d, %d items" % (iterator.next_cursor, len(page['lists'])))
                for _list in page['lists']:
                    TwitterList.objects(user_id=USER_ID, list_id=_list['id']).modify(upsert=True, new=True,
                                                                                     set__data=_list)
                    logger.info("Add %s - %s" % (USER_ID, _list['id']))
                next_cursor = iterator.next_cursor

            logger.info("FINISHED!! Current objects : %s" % len(TwitterList.objects))
            logger.info("Use time total %s" % (datetime.now() - start_time))
            finish = True
        except tweepy.error.TweepError as error:
            logger.info("Tweepy Error %s" % error)
            logger.info("Retry in 60 secs")
            sleep(60)
        except requests.exceptions.ConnectionError as error:
            logger.error("Request Error : %s" % error)
            logger.info("Retry in 60 secs")
            sleep(60)
        except ServerSelectionTimeoutError as error:
            logger.error("Cannot connect to database : %s" % error)
            logger.info("Retry in 60 secs")
            sleep(60)

if __name__ == "__main__":
    main()
