FROM python:2.7

WORKDIR /usr/src/app/

COPY source .
COPY requirements.txt .
RUN pip install -r ./requirements.txt
RUN chmod +x entrypoint.sh

ENTRYPOINT ["./entrypoint.sh"]
